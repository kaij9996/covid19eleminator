# WeOffer
## Thema
App für die Vermittlung von Hilfe in COVID-19 Zeiten
## Gruppe
Khaled Jebrini & Mustafa Mado
## Sprache
Java
## Must have:
    1. Die Gelegenheit verschiedene Hilfe-Arten anzubieten.
    2. Die Möglichkeit Hilfe zu suchen. 
    3. Die App Kategorisiert die Art des Angebotes.
    4. Die App soll die Referenzen der aktuellen COVID-19 Lage als Hyperlinks hinterlegen.
    5. Count-Down für die Quarantäne-Zeit.
    6. Die App muss auf Deutsch sein.
    7. Die User sollen mit ein paar Klicks Kontakt mit Hilfe-Anbietenden aufzunehmen.
## Nice to Have
    1. 
    2. 



